import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
    cy.visit('http://localhost:8080');

    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, () => {
    cy.get('#goToPayment').click();
});

When(/^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/, () => {
    cy.get('#fullName').type('Helge Bjørløw');
    cy.get('#address').type('Eikerveien 64');
    cy.get('#postCode').type('3612');
    cy.get('#city').type('kongsberg');
    cy.get('#creditCardNo').type('1231231231231231');
});

And(/^trykker på Fullfør kjøp$/, () => {
    cy.get('input[type=submit]').click();
});

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, () => {
    cy.get('.confirmation').contains('Din ordre er registrert')
});


Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
    cy.visit('http://localhost:8080');

    cy.get('#product').select('Hubba Gubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, () => {
    cy.get('#goToPayment').click();
});

When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
    cy.get('#creditCardNo').type('abc');
});

Then(/^skal jeg få feilmeldinger for disse$/, () => {
        //attempt to submit order with no info and validate error on name field
        cy.get('input[type=submit]').click();
        cy.get('#fullNameError').should('have.text', 'Feltet må ha en verdi')
        cy.get('#fullName').type('Helge Bjørløw');

        //attempt to submit order with name info and validate error on address field
        cy.get('input[type=submit]').click();
        cy.get('#addressError').should('have.text', 'Feltet må ha en verdi')
        cy.get('#address').type('Eikerveien 64');

        //attempt to submit order with name and address and validate error on postcode field
        cy.get('input[type=submit]').click();
        cy.get('#postCodeError').should('have.text', 'Feltet må ha en verdi')
        cy.get('#postCode').type('3612');

        //attempt to submit order with name, address, postcode and validate error on postcode field
        cy.get('input[type=submit]').click();
        cy.get('#cityError').should('have.text', 'Feltet må ha en verdi')
        cy.get('#city').type('Kongsberg');

        //attempt to submit order with name, address, postcode, city and validate error on creditcardnumber field
        cy.get('input[type=submit]').click();
        cy.get('#creditCardNoError').should('have.text', 'Kredittkortnummeret må bestå av 16 siffer')
        cy.get('#creditCardNo').type('abcabcabcabcabcc');
        cy.get('#creditCardNoError').should('have.text', 'Kredittkortnummeret må bestå av 16 siffer')
        cy.get('#creditCardNo').clear().type('123123123123');
        cy.get('#creditCardNoError').should('have.text', 'Kredittkortnummeret må bestå av 16 siffer')
        cy.get('#creditCardNo').clear().type('123123123123123123123123123');
        cy.get('#creditCardNoError').should('have.text', 'Kredittkortnummeret må bestå av 16 siffer')
});
