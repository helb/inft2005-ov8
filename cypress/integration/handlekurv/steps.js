import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

When(/^jeg legger inn varer og kvanta$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

Then(/^skal handlekurven inneholde det jeg har lagt inn$/, () => {
    cy.get('#cart').contains('ul', '4 Hubba bubba; 8 kr');
    cy.get('#cart').contains('ul', '5 Smørbukk; 5 kr');
    cy.get('#cart').contains('ul', '1 Stratos; 8 kr');
    cy.get('#cart').contains('ul', '2 Hobby; 12 kr');
});

And(/^den skal ha riktig totalpris$/, function () {
        cy.get('#price').should('have.text', '33');
});


Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

And(/^jeg legger inn varer og kvanta$/, function () {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

When(/^jeg sletter varer$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4')
    cy.get('#deleteItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1')
    cy.get('#deleteItem').click();
});

Then(/^skal ikke handlekurven inneholde det jeg har slettet$/, () => {
    cy.get('#cart').contains('ul', '4 Hubba Bubba; 8 kr').should('not.exist')
    cy.get('#cart').contains('ul', '1 Stratos; 8 kr').should('not.exist');
    cy.get('#cart').contains('ul', '2 Hobby; 12 kr');
    cy.get('#cart').contains('ul', '5 Smørbukk; 5 kr');
});


Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

And(/^lagt inn varer og kvanta$/, function () {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});


When(/^jeg oppdaterer kvanta for en vare$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('2')
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('2')
    cy.get('#saveItem').click();
});


Then(/^skal handlekurven inneholde riktig kvanta for varen$/, () => {
    cy.get('#cart').contains('ul', '2 Hubba bubba; 4 kr');
    cy.get('#cart').contains('ul', '2 Smørbukk; 2 kr');
    cy.get('#cart').contains('ul', '1 Stratos; 8 kr');
    cy.get('#cart').contains('ul', '2 Hobby; 12 kr');
});

